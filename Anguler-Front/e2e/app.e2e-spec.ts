import { AngulerFrontPage } from './app.po';

describe('anguler-front App', () => {
  let page: AngulerFrontPage;

  beforeEach(() => {
    page = new AngulerFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
