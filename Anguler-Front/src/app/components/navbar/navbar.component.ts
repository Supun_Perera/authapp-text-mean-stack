import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private router:Router,
    private autservice:AuthService,
    private flashMessagesService:FlashMessagesService
  ) { }

  ngOnInit() {
  }

  logoutUser(){
    this.autservice.logout();
    this.flashMessagesService.show("You are logout", {cssClass: 'alert-success', timeout: 3000});
    this.router.navigate(['/login']);
    return false;
  }
  /*loggedIn() {
    const s= this.autservice.loggedIn();
    console.log(s);
    return s;
  }
*/

}
