import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';

import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  user:any;

  authToken:any;

  constructor(
    private http:Http
  ) { }
  registerUser(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');

    return this.http.post("http://localhost:3000/register",user,{headers:headers}).map(res=>res.json());
  }

  lodinUser(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');

    return this.http.post("http://localhost:3000/login",user,{headers:headers}).map(res=>res.json());

  }

  getprofile(){

    this.fetchToken();

    let headers = new Headers();

    headers.append('Authorization',this.authToken);
    headers.append('Content-Type','application/json');

    return this.http.get("http://localhost:3000/profile",{headers:headers}).map(res=>res.json());

  }

  fetchToken(){
    const token = localStorage.getItem("tokenid");
    this.authToken = token;
  }

  storeData(token, userData){
    localStorage.setItem("tokenid",token);
    localStorage.setItem("user",JSON.stringify(userData));

    this.authToken = token;
    this.user = userData;

  }

  loggedIn() {
    return tokenNotExpired('tokenid');
  }

  logout(){
    this.authToken=null;
    this.user=null;
    localStorage.clear();
  }


}
